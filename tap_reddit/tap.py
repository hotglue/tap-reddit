"""Reddit tap class."""

from typing import List

from singer_sdk import Stream, Tap
from singer_sdk import typing as th

from tap_reddit.streams import CommentsStream, SubRedditsStream

STREAM_TYPES = [SubRedditsStream, CommentsStream]


class TapReddit(Tap):
    """Reddit tap class."""

    name = "tap-reddit"

    def __init__(
        self,
        config=None,
        catalog=None,
        state=None,
        parse_env_config=False,
        validate_config=True,
    ) -> None:
        self.config_file = config[0]
        super().__init__(config, catalog, state, parse_env_config, validate_config)

    config_jsonschema = th.PropertiesList(
        th.Property(
            "client_id",
            th.StringType,
            required=True,
        ),
        th.Property(
            "client_secret",
            th.StringType,
            required=True,
        ),
        th.Property(
            "refresh_token",
            th.StringType,
            required=True,
        ),
        th.Property(
            "subreddit",
            th.StringType,
            required=True,
        ),
        th.Property(
            "redirect_uri",
            th.StringType,
            default="https://hotglue.com/callback",
        ),
        th.Property(
            "user_agent",
            th.StringType,
            default="Python: Hotglue:0.01 by (/u/hotglue) ",
        ),
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]


if __name__ == "__main__":
    TapReddit.cli()
