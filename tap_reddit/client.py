"""REST client handling, including RedditStream base class."""

import copy
import time
from pathlib import Path
from typing import Any, Dict, Iterable, Optional

import requests
from memoization import cached
from singer_sdk.helpers.jsonpath import extract_jsonpath
from singer_sdk.streams import RESTStream

from tap_reddit.auth import OAuth2Authenticator

SCHEMAS_DIR = Path(__file__).parent / Path("./schemas")


class RedditStream(RESTStream):
    """Reddit stream class."""

    url_base = "https://oauth.reddit.com"
    _page_size = 100

    records_jsonpath = "$.data.children[*]"  # Or override `parse_response`.
    next_page_token_jsonpath = "$.data.after"  # Or override `get_next_page_token`.

    @property
    @cached
    def authenticator(self) -> OAuth2Authenticator:
        """Return a new authenticator object."""
        return OAuth2Authenticator(
            self,
            auth_endpoint="https://www.reddit.com/api/v1/access_token",
        )

    @property
    def http_headers(self) -> dict:
        """Return the http headers needed."""
        headers = {}
        if "user_agent" in self.config:
            headers["User-Agent"] = self.config.get("user_agent")
        return headers

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:
        """Return a token for identifying next page or None if no more pages."""

        # Check and apply rate limit
        limit_remaining = int(float(response.headers["x-ratelimit-remaining"]))
        limit_reset_in = int(float(response.headers["x-ratelimit-reset"]))
        if limit_remaining <= 2:
            time.sleep(limit_reset_in)

        result = response.json()
        if self.tap_stream_id == "comments":
            result = result[1]

        if self.next_page_token_jsonpath:
            all_matches = extract_jsonpath(
                self.next_page_token_jsonpath, response.json()
            )
            first_match = next(iter(all_matches), None)
            next_page_token = first_match
        else:
            next_page_token = response.headers.get("X-Next-Page", None)

        return next_page_token

    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:
        """Return a dictionary of values to be used in URL parameterization."""
        params: dict = {}
        if next_page_token:
            params["after"] = next_page_token

        return params

    def post_process(self, row: dict, context: Optional[dict]) -> dict:
        """As needed, append or transform raw data to match expected structure."""
        # TODO: Delete this method if not needed.
        return_row = {}
        return_row["kind"] = row["kind"]
        return_row.update(row["data"])
        return return_row

    def get_url(self, context: Optional[dict]) -> str:

        if self.tap_stream_id == "subreddits":
            return f"{self.url_base}/r/{self.config.get('subreddit')}"

        url = "".join([self.url_base, self.path or ""])

        vals = copy.copy(dict(self.config))
        vals.update(context or {})
        for k, v in vals.items():
            search_text = "".join(["{", k, "}"])
            if search_text in url:
                url = url.replace(search_text, v)
        return url
